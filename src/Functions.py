import time
from Controls import set_target_station, stations_in_reach, buy, Calculate_Maximum_Amount_to_buy, get_amount_of_Resource, sell


def AutoTrade(BuyStation, SellStation, Resource):
    #BuyStation = "Vesta Station"
    #SellStation = "Core Station"
    #Resource = "IRON"

    set_target_station(BuyStation)
    time.sleep(15)

    while True:
        Station = stations_in_reach()
        time.sleep(1)
        print(Station)

        if (BuyStation in stations_in_reach()):
            buy(Resource, BuyStation,
                Calculate_Maximum_Amount_to_buy(BuyStation, Resource))
            break

    set_target_station(SellStation)
    time.sleep(15)

    while True:
        Station = stations_in_reach()
        time.sleep(1)
        print(Station)

        if (SellStation in stations_in_reach()):
            sell(Resource, SellStation, get_amount_of_Resource(Resource))
            break
