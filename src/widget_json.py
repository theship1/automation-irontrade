import json


def autofarmwidget(button1, button2):

    return json.dumps({
        'kind': 'update_widget',
        'widget': {
            "title": "Automatet Iron Traiding V2",
            "width": 2,
            "height": 2,
            "doc":
            "\n# Example use\n```shell\n$ curl -XPUT http://192.168.0.3:2004/thruster --data '{\"thrust_percent\": 0}'\n$ curl -XPUT http://192.168.0.3:2004/thruster --data '{\"thrust_percent\": 50}'\n$ curl -XPUT http://192.168.0.3:2004/thruster --data '{\"thrust_percent\": 100}'\n```\n",
            "content": {
                "kind":
                "stack",
                "content": [{
                    "kind":
                    "text",
                    "text":
                    "Wehen the on button is pressed a script for automatet iron traings will start"
                }, {
                    "kind": "button",
                    "id": "automate-ironfarm-off",
                    "text": "OFF",
                    "pressed": button1
                }, {
                    "kind": "button",
                    "id": "automate-ironfarm-on",
                    "text": "ON",
                    "pressed": button2
                }]
            }
        }
    })
