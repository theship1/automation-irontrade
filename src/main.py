#!/usr/bin/python3
import socket
import json
from threading import Thread

from Functions import AutoTrade
from widget_json import autofarmwidget

with open(__file__, 'r') as f:
    this_file = f.read()
doc = '```python\n' + this_file + '\n```'

status = False
t1 = None
#https://realpython.com/python-sockets/
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    #s.connect((os.environ['K8S_HOST'], 2002))
    s.connect(("192.168.0.3", 2002))
    s.sendall(autofarmwidget(status == False, status).encode('utf-8'))
    s.sendall(b'\0')  # null-byte -> end of message

    buffer = bytes()
    while True:
        received = s.recv(1)
        if received == b'\0':
            decodbuffer = buffer.decode('utf-8')

            data = json.loads(decodbuffer)
            kind = data.get("kind")
            print(kind)

            if (kind == "button_pressed"):
                status = status == False
                s.sendall(
                    autofarmwidget(status == False, status).encode('utf-8'))
                s.sendall(b'\0')

            if status:
                if t1 == None:
                    t1 = Thread(target=AutoTrade,
                                args=(
                                    "Vesta Station",
                                    "Core Station",
                                    "IRON",
                                ))
                    t1.start()

                if t1.is_alive():
                    print("thread is running")
                else:
                    t1 = Thread(target=AutoTrade,
                                args=(
                                    "Vesta Station",
                                    "Core Station",
                                    "IRON",
                                ))
                    t1.start()

            buffer = bytes()
        else:
            buffer += received
