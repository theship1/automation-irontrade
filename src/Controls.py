import requests
import json


def set_target_station(name):
    url = 'http://192.168.0.3:2009/set_target'
    body = json.dumps({"target": name})

    requests.post(url, body)


def stations_in_reach():
    url = 'http://192.168.0.3:2011/stations_in_reach'
    return json.loads(
        requests.get(url).content.decode('utf8').replace("'", '"'))["stations"]


def buy(resource, station, amount):
    url = 'http://192.168.0.3:2011/buy'
    body = json.dumps({"station": station, "what": resource, "amount": amount})
    requests.post(url, body)


def sell(resource, station, amount):
    url = 'http://192.168.0.3:2011/sell'
    body = json.dumps({"station": station, "what": resource, "amount": amount})
    requests.post(url, body)


def buy_Price(Station, Resource):
    StationInfo = stations_in_reach()
    return StationInfo[Station]["resources"][Resource]["buy_price"]


def Calculate_Maximum_Amount_to_buy(Station, Resource):
    url = 'http://192.168.0.3:2012/hold'
    ShipInfo = json.loads(
        requests.get(url).content.decode('utf8').replace("'", '"'))
    MaxAmountWithCredits = ShipInfo["hold"]["credits"] / buy_Price(
        Station, Resource)

    if MaxAmountWithCredits < ShipInfo["hold"]["hold_free"]:
        return MaxAmountWithCredits
    return ShipInfo["hold"]["hold_free"]


def get_amount_of_Resource(Resource):
    url = 'http://192.168.0.3:2012/hold'
    ShipInfo = json.loads(
        requests.get(url).content.decode('utf8').replace("'", '"'))
    return ShipInfo["hold"]["resources"][Resource]


print(stations_in_reach().keys())

print("Core Station" in stations_in_reach())
